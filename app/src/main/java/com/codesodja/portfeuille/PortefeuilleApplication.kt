package com.codesodja.portfeuille

import android.app.Application
import com.codesodja.portfeuille.data.PortefeuilleRoomDatabase
import com.codesodja.portfeuille.data.repository.OperationRepository
import com.codesodja.portfeuille.data.repository.TypeOperationRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class PortefeuilleApplication: Application() {

    private val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { PortefeuilleRoomDatabase.getDatabase(this, applicationScope) }
    val operationRepository by lazy { OperationRepository(database.operationDao()) }
    val typeOperationRepository by lazy { TypeOperationRepository(database.typeOperationDao()) }
}