package com.codesodja.portfeuille.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "type_operation")
data class TypeOperation (
    @PrimaryKey(autoGenerate = true)
    val idTypeOperation: Long,
    val name: String
)