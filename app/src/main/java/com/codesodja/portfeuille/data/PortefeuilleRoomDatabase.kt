package com.codesodja.portfeuille.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.codesodja.portfeuille.data.dao.OperationDao
import com.codesodja.portfeuille.data.dao.TypeOperationDao
import com.codesodja.portfeuille.data.entities.Operation
import com.codesodja.portfeuille.data.entities.TypeOperation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [Operation::class, TypeOperation::class], version = 1, exportSchema = false)
abstract class PortefeuilleRoomDatabase : RoomDatabase() {

    abstract fun operationDao(): OperationDao
    abstract fun typeOperationDao(): TypeOperationDao


    private class TypeOperationDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.typeOperationDao())
                }
            }
        }

        suspend fun populateDatabase(typeOperationDao: TypeOperationDao) {
            // Delete all content here.
            typeOperationDao.deleteAllTypeOperation()

            // Add sample words.
            var typeOperation = TypeOperation(0, "prevision")
            typeOperationDao.insertTypeOperation(typeOperation)
            typeOperation = TypeOperation(0, "depense")
            typeOperationDao.insertTypeOperation(typeOperation)
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: PortefeuilleRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): PortefeuilleRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PortefeuilleRoomDatabase::class.java,
                    "rally_database"
                ).addCallback(TypeOperationDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}