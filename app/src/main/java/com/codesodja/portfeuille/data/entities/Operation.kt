package com.codesodja.portfeuille.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "operation")
data class Operation(
    @PrimaryKey(autoGenerate = true)
    val idOperation: Long,
    val name: String,
    val due: String,
    val amount: Float,
    val color: String,
    val idTypeOperation: Long
)
