package com.codesodja.portfeuille.data.entities

import androidx.room.Embedded
import androidx.room.Relation

data class TypeOperationWithOperations(
    @Embedded val typeOperation: TypeOperation,
    @Relation(
        parentColumn = "idTypeOperation",
        entityColumn = "idTypeOperation"
    )
    val operations: List<Operation>
)
