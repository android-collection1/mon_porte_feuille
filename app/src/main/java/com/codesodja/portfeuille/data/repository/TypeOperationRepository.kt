package com.codesodja.portfeuille.data.repository

import androidx.annotation.WorkerThread
import com.codesodja.portfeuille.data.entities.TypeOperation
import com.codesodja.portfeuille.data.dao.TypeOperationDao
import kotlinx.coroutines.flow.Flow

class TypeOperationRepository(private val typeOperationDao: TypeOperationDao) {
    val allTypeOperation: Flow<List<TypeOperation>> = typeOperationDao.getAllTypeOperation()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(typeOperation: TypeOperation) {
        typeOperationDao.insertTypeOperation(typeOperation)
    }
}