package com.codesodja.portfeuille.data.dao

import androidx.room.*
import com.codesodja.portfeuille.data.entities.TypeOperation
import kotlinx.coroutines.flow.Flow

@Dao
interface TypeOperationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTypeOperation(typeOperation: TypeOperation)

    @Update
    suspend fun updateTypeOperation(typeOperation: TypeOperation)

    @Delete
    suspend fun deleteTypeOperation(typeOperation: TypeOperation)

    @Query("DELETE FROM type_operation")
    suspend fun deleteAllTypeOperation()

    @Query("SELECT * FROM type_operation")
    fun getAllTypeOperation(): Flow<List<TypeOperation>>

    @Query("SELECT * FROM type_operation WHERE idTypeOperation =:idTypeOperation")
    fun getTypeOperation(idTypeOperation: Long): Flow<TypeOperation>

    @Query("SELECT * FROM type_operation WHERE idTypeOperation = :idTypeOperation")
    fun getTypeOperationByIdTypeOperation(idTypeOperation: Long): Flow<List<TypeOperation>>
}