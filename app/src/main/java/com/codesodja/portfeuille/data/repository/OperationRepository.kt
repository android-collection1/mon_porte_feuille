package com.codesodja.portfeuille.data.repository

import androidx.annotation.WorkerThread
import com.codesodja.portfeuille.data.entities.Operation
import com.codesodja.portfeuille.data.dao.OperationDao
import com.codesodja.portfeuille.data.entities.TypeOperationWithOperations
import kotlinx.coroutines.flow.Flow

class OperationRepository(private val operationDao: OperationDao) {

    val allOperation: Flow<List<Operation>> = operationDao.getAllOperation()

    @WorkerThread
    fun getOperationByTypeOperation(id: Long): Flow<List<TypeOperationWithOperations>>{
        return operationDao.getOperationWithTypeOperation(id)
    }

    @WorkerThread
    fun getLastOperationWithTypeOperation(id: Long): Flow<List<Operation>>{
        return operationDao.getLastOperationWithTypeOperation(id)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(operation: Operation): Long {
       return operationDao.insertOperation(operation)
    }

}