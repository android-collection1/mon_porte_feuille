package com.codesodja.portfeuille.data.dao

import androidx.room.*
import com.codesodja.portfeuille.data.entities.Operation
import com.codesodja.portfeuille.data.entities.TypeOperationWithOperations
import kotlinx.coroutines.flow.Flow

@Dao
interface OperationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOperation(operation: Operation): Long

    @Update
    suspend fun updateOperation(operation: Operation)

    @Delete
    suspend fun deleteOperation(operation: Operation)

    @Query("DELETE FROM operation")
    suspend fun deleteAllOperation()

    @Query("SELECT * FROM operation")
    fun getAllOperation(): Flow<List<Operation>>

    @Query("SELECT * FROM operation WHERE idOperation = :idOperation")
    fun getOperationByIdOperation(idOperation: Long): Flow<Operation>

    @Transaction
    @Query("SELECT * FROM type_operation WHERE idTypeOperation = :typeOperationId")
    fun getOperationWithTypeOperation(typeOperationId: Long): Flow<List<TypeOperationWithOperations>>

    @Transaction
    @Query("SELECT * FROM operation WHERE idTypeOperation = :typeOperationId ORDER BY idOperation DESC LIMIT 3 ")
    fun getLastOperationWithTypeOperation(typeOperationId: Long): Flow<List<Operation>>
}