package com.codesodja.portfeuille.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.codesodja.portfeuille.R
import com.codesodja.portfeuille.data.entities.TypeOperation

class SpinnerAdapter(context: Context, typeOperations: List<TypeOperation>) :
    ArrayAdapter<TypeOperation>(context, 0, typeOperations) {

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {

        val type = getItem(position)

        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_item,
            parent,
            false
        )

        val spinner = view.findViewById<TextView>(R.id.spinner_item)
        type!!.name.also { spinner.text = it }

        return view
    }
}