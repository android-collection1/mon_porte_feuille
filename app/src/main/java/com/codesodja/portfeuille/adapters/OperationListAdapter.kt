package com.sodja.rally.adapters

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.codesodja.portfeuille.R
import com.codesodja.portfeuille.data.entities.Operation

class OperationListAdapter: ListAdapter<Operation, OperationListAdapter.OperationViewHolder>(OperationComparator()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OperationViewHolder {
        return OperationViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: OperationViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    class OperationViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val operationLabel: TextView = itemView.findViewById(R.id.txt_operation_label)
        private val operationDesc: TextView = itemView.findViewById(R.id.txt_operation_desc)
        private val operationCurrency: TextView = itemView.findViewById(R.id.txt_currency)
        private val operationAmount: TextView = itemView.findViewById(R.id.txt_amount)
        private val operationColor: View = itemView.findViewById(R.id.vertical_color_line)

        fun bind(operation: Operation) {
            operationLabel.text = operation.name
            operationDesc.text = operation.due
            operationCurrency.text = "$"
            operationAmount.text = operation.amount.toString()
         //   operationColor.setBackgroundColor(Color.parseColor(operation.color+"00"))
        }

        companion object {
            fun create(parent: ViewGroup): OperationViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_items, parent, false)
                return OperationViewHolder(view)
            }
        }
    }

    class OperationComparator : DiffUtil.ItemCallback<Operation>() {
        override fun areItemsTheSame(oldItem: Operation, newItem: Operation): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Operation, newItem: Operation): Boolean {
            return oldItem.idOperation == newItem.idOperation
        }
    }
}