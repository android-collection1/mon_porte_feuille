package com.codesodja.portfeuille.ui.views.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codesodja.portfeuille.PortefeuilleApplication
import com.codesodja.portfeuille.R
import com.codesodja.portfeuille.ui.viewmodels.RecapitulatifViewModel
import com.codesodja.portfeuille.ui.viewmodels.RecapitulatifViewModelFactory
import com.sodja.rally.adapters.OperationListAdapter

class RecapitulatifFragment : Fragment() {

    companion object {
        fun newInstance() = RecapitulatifFragment()
    }

    private var total: Double = 0.0
    private lateinit var viewModel: RecapitulatifViewModel
    private lateinit var accountBloc: View
    private lateinit var billBloc: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val recaputilatifView: View =
            inflater.inflate(R.layout.recapitulatif_fragment, container, false)


        accountBloc = recaputilatifView.findViewById(R.id.account_bloc)
        billBloc = recaputilatifView.findViewById(R.id.bill_blosc)

        val billTotal =  billBloc.findViewById<TextView>(R.id.txt_bill_card_amount)
        val accountTotal =  accountBloc.findViewById<TextView>(R.id.txt_account_card_amount)


        val accountRecyclerView =
            accountBloc.rootView.findViewById<RecyclerView>(R.id.recap_account_recyclerview)
        val accountAdapter = OperationListAdapter()
        accountRecyclerView.adapter = accountAdapter
        accountRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        val billRecyclerView =
            billBloc.rootView.findViewById<RecyclerView>(R.id.recap_bill_recyclerview)
        val billAdapter = OperationListAdapter()
        billRecyclerView.adapter = billAdapter
        billRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        // VIEW MODEL FACTORY INITIALIZR
        val typeOperationRepository =
            (requireActivity().application as PortefeuilleApplication).typeOperationRepository
        val operationRepository =
            (requireActivity().application as PortefeuilleApplication).operationRepository
        val factory = RecapitulatifViewModelFactory(typeOperationRepository, operationRepository)

        viewModel = ViewModelProvider(this, factory).get(RecapitulatifViewModel::class.java)

        viewModel.bills.observe(requireActivity(), Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let {listBills->
                Log.d("liste bill", listBills.toString())
                billTotal.text = listBills.sumByDouble { it.amount.toDouble() }.toString()
                billAdapter.submitList(listBills)
            }
        })

        viewModel.account.observe(requireActivity(), Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let {listAccount->
                Log.d("liste", listAccount.toString())
                accountTotal.text = listAccount.sumByDouble { it.amount.toDouble() }.toString()
                accountAdapter.submitList(listAccount)
            }
        })

        return recaputilatifView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}