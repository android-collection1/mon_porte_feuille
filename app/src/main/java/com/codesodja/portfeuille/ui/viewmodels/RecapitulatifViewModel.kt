package com.codesodja.portfeuille.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.codesodja.portfeuille.data.entities.Operation
import com.codesodja.portfeuille.data.entities.TypeOperation
import com.codesodja.portfeuille.data.repository.OperationRepository
import com.codesodja.portfeuille.data.repository.TypeOperationRepository
import kotlinx.coroutines.launch

class RecapitulatifViewModel(private val typeOperationRepository: TypeOperationRepository,
                             private val operationRepository: OperationRepository
) : ViewModel() {

    val allOperation: LiveData<List<TypeOperation>> = typeOperationRepository.allTypeOperation.asLiveData()

    val bills: LiveData<List<Operation>> = operationRepository.getLastOperationWithTypeOperation(2).asLiveData()
    val account: LiveData<List<Operation>> = operationRepository.getLastOperationWithTypeOperation(1).asLiveData()

   /* fun insert(operation: Operation) = viewModelScope.launch {
        operationRepository.insert(operation)
    }*/

}