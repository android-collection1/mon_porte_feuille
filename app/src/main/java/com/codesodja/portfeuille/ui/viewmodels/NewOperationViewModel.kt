package com.codesodja.portfeuille.ui.viewmodels

import androidx.lifecycle.*
import com.codesodja.portfeuille.data.entities.Operation
import com.codesodja.portfeuille.data.entities.TypeOperation
import com.codesodja.portfeuille.data.repository.OperationRepository
import com.codesodja.portfeuille.data.repository.TypeOperationRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class NewOperationViewModel(private val typeOperationRepository: TypeOperationRepository,
                            private val operationRepository: OperationRepository
) : ViewModel() {

    val allTypeOperation: LiveData<List<TypeOperation>> = typeOperationRepository.allTypeOperation.asLiveData()
    val allOperation: LiveData<List<Operation>> = operationRepository.allOperation.asLiveData()

    val insertResult = MutableLiveData<Long>()

     fun insert(operation: Operation) = viewModelScope.launch {
         insertResult.postValue(operationRepository.insert(operation))
   }

}