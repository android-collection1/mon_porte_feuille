package com.codesodja.portfeuille.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.codesodja.portfeuille.data.repository.OperationRepository
import com.codesodja.portfeuille.data.repository.TypeOperationRepository

class NewOperationViewModelFactory (private val typeOperationRepository: TypeOperationRepository, private val operationRepository: OperationRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewOperationViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return NewOperationViewModel(typeOperationRepository, operationRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}