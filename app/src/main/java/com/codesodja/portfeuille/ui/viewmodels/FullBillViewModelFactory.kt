package com.codesodja.portfeuille.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.codesodja.portfeuille.data.repository.OperationRepository
import com.codesodja.portfeuille.data.repository.TypeOperationRepository

class FullBillViewModelFactory (private val typeOperationRepository: TypeOperationRepository, private val operationRepository: OperationRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FullBillViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return FullBillViewModel(typeOperationRepository, operationRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}