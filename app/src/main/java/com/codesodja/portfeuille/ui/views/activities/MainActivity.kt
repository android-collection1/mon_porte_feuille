package com.codesodja.portfeuille.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.DEBUG
import android.view.View
import android.widget.Toast
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import com.codesodja.portfeuille.R
import com.google.android.material.tabs.TabLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.codesodja.portfeuille.BuildConfig.DEBUG
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var appBar: AppBarLayout
    private lateinit var tabLayout: TabLayout
    private lateinit var addOperation: FloatingActionButton
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initVariables()

        tabLayout.getTabAt(0)?.select()

        clickManagement()
    }

    private fun clickManagement() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabNavigation(tab)
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Handle tab reselect
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                clearTabItemText(tab)
            }
        })

        addOperation.setOnClickListener(this)
    }

    private fun initVariables() {
        tabLayout = findViewById(R.id.tab_layout)
        addOperation = findViewById(R.id.add_button)
        appBar = findViewById(R.id.appbar)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }

    private fun clearTabItemText(tab: TabLayout.Tab?) {
        when (tab?.position) {
            0 -> {
                tab.text = ""
            }
            1 -> {
                tab.text = ""
            }
            2 -> {
                tab.text = ""
            }
            else -> {
                print("x is neither 1 nor 2")
            }
        }
    }

    private fun tabNavigation(tab: TabLayout.Tab?) {
        when (tab?.position) {
            0 -> {
                tab.text = "Overview"
                navigationManager(false, R.id.recapitulatifFragment)
            }
            1 -> {
                tab.text = "Prévision"
                navigationManager(true, R.id.fullAccountFragment)
            }
            2 -> {
                tab.text = "Depense"
                navigationManager(true, R.id.fullBillFragment)
            }
            else -> {
                print("x is neither 1 nor 2")
            }
        }
    }

    private fun navigationManager(isPopBackStack: Boolean, destination: Int) {
        if (isPopBackStack){
            navController.popBackStack()
        }
        navController.navigate(destination)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.add_button->{
                if(navController.currentDestination?.label != "new_operation_fragment"){
                    navController.navigate(R.id.newOperationFragment)
                    appBar.visibility = View.GONE
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(navController.currentDestination?.label == "recapitulatif_fragment"){
            appBar.visibility = View.VISIBLE
        }
    }
}