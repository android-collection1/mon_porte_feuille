package com.codesodja.portfeuille.ui.views.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codesodja.portfeuille.PortefeuilleApplication
import com.codesodja.portfeuille.R
import com.codesodja.portfeuille.ui.viewmodels.FullBillViewModel
import com.codesodja.portfeuille.ui.viewmodels.FullBillViewModelFactory
import com.sodja.rally.adapters.OperationListAdapter

class FullBillFragment : Fragment() {

    companion object {
        fun newInstance() = FullBillFragment()
    }
    private var status = 0
    private lateinit var progressBar: ProgressBar
    private val handler = Handler()
    lateinit var textView: TextView
    private lateinit var viewModel: FullBillViewModel

    private var total: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fullBillView = inflater.inflate(R.layout.full_bill_fragment, container, false)

        // VIEW MODEL FACTORY INITIALIZR
        val typeOperationRepository =
            (requireActivity().application as PortefeuilleApplication).typeOperationRepository
        val operationRepository =
            (requireActivity().application as PortefeuilleApplication).operationRepository
        val factory = FullBillViewModelFactory(typeOperationRepository, operationRepository)

        viewModel = ViewModelProvider(this, factory).get(FullBillViewModel::class.java)

        val recyclerView = fullBillView.findViewById<RecyclerView>(R.id.bill_recyclerview)
        val billAdapter = OperationListAdapter()
        recyclerView.adapter = billAdapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        viewModel.bills.observe(requireActivity(), Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let {listOperation->
                Log.d("bill liste", listOperation[0].operations.toString())
                this.total = listOperation[0].operations.sumByDouble { it.amount.toDouble() }
                Log.d("total", this.total.toString())
                createAnimator(fullBillView)
                billAdapter.submitList(listOperation[0].operations)
            }
        })
        return fullBillView
    }

    private fun createAnimator(fullBillView: View) {
        Log.d("total++", this.total.toString())
        val resources = resources
        val drawable = resources.getDrawable(R.drawable.circularprogressbar, null)
        progressBar = fullBillView.findViewById(R.id.circularProgressbar)
        progressBar.progress = 0
        progressBar.max = 300
        progressBar.progressDrawable = drawable
        textView = fullBillView.findViewById(R.id.txt_bill_card_total_amount)
        Thread {
            while (status < 300) {
                status += 5
                handler.post {
                    progressBar.progress = status
                    textView.text = "$total F CFA"
                }
                try {
                    Thread.sleep(16)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }.start()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
       // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}