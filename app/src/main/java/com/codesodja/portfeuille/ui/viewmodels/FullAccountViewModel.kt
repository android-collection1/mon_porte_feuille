package com.codesodja.portfeuille.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.codesodja.portfeuille.data.entities.TypeOperationWithOperations
import com.codesodja.portfeuille.data.repository.OperationRepository
import com.codesodja.portfeuille.data.repository.TypeOperationRepository

class FullAccountViewModel (private val typeOperationRepository: TypeOperationRepository,
                            private val operationRepository: OperationRepository
): ViewModel() {
    val account: LiveData<List<TypeOperationWithOperations>> = operationRepository.getOperationByTypeOperation(1).asLiveData()
}