package com.codesodja.portfeuille.ui.views.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Spinner
import android.widget.Switch
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.codesodja.portfeuille.PortefeuilleApplication
import com.codesodja.portfeuille.R
import com.codesodja.portfeuille.adapters.SpinnerAdapter
import com.codesodja.portfeuille.data.entities.Operation
import com.codesodja.portfeuille.data.entities.TypeOperation
import com.codesodja.portfeuille.ui.viewmodels.NewOperationViewModel
import com.codesodja.portfeuille.ui.viewmodels.NewOperationViewModelFactory
import com.google.android.material.textfield.TextInputEditText


class NewOperationFragment : Fragment(), View.OnClickListener {

    companion object {
        fun newInstance() = NewOperationFragment()
    }

    private lateinit var txtInputName: TextInputEditText
    private lateinit var txtInputAmount: TextInputEditText
    private lateinit var txtInputDue: TextInputEditText
    private lateinit var validatedButton: Button
    private lateinit var depenseSwitch: Switch
    private lateinit var previsionSwitch: Switch

    private lateinit var viewModel: NewOperationViewModel

    private lateinit var dialog: AlertDialog
    private var nameIsNotEmpty = false
    private var amountIsNotEmpty = false
    private var dueIsNotEmpty = false
    private var typeOps = 2L

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val newOperationView = inflater.inflate(R.layout.new_operation_fragment, container, false)

        // VIEW MODEL FACTORY INITIALIZR
        val typeOperationRepository =
            (requireActivity().application as PortefeuilleApplication).typeOperationRepository
        val operationRepository =
            (requireActivity().application as PortefeuilleApplication).operationRepository
        val factory = NewOperationViewModelFactory(typeOperationRepository, operationRepository)

        viewModel = ViewModelProvider(this, factory).get(NewOperationViewModel::class.java)
        txtInputAmount = newOperationView.findViewById(R.id.txt_input_edittext_amount)
        txtInputDue = newOperationView.findViewById(R.id.txt_input_edittext_due)
        txtInputName = newOperationView.findViewById(R.id.txt_input_edittext_name)
        validatedButton = newOperationView.findViewById(R.id.validated_button)
        depenseSwitch = newOperationView.findViewById(R.id.bill)
        previsionSwitch = newOperationView.findViewById(R.id.preview)

        textListener()

        /* val llProgressBar: View = newOperationView.findViewById(R.id.llProgressBar)
         llProgressBar.visibility = View.VISIBLE
 */
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setCancelable(false) // if you want user to wait for some process to finish,

        builder.setView(R.layout.loading)
        dialog = builder.create()
        // to show this dialog

        /*val progressDialog = ProgressDialog(requireActivity())
        progressDialog.setTitle("Kotlin Progress Bar")
        progressDialog.setMessage("Application is loading, please wait")
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show()*/

        validatedButton.setOnClickListener(this)
        depenseSwitch.setOnClickListener(this)
        previsionSwitch.setOnClickListener(this)

        viewModel.allTypeOperation.observe(requireActivity(), Observer { typeOperation ->
            // Update the cached copy of the words in the adapter.
            typeOperation?.let { types ->
                Log.d("my liste", types.toString())
            }
        })

        return newOperationView
    }

    private fun textListener() {
        txtInputName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                nameIsNotEmpty = s.toString().isNotEmpty()
                enableValidateButton()
            }

            override fun afterTextChanged(s: Editable?) {}

        })
        txtInputAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                amountIsNotEmpty = s.toString().isNotEmpty()
                enableValidateButton()
            }

            override fun afterTextChanged(s: Editable?) {}

        })
        txtInputDue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                dueIsNotEmpty = s.toString().isNotEmpty()
                enableValidateButton()
            }

            override fun afterTextChanged(s: Editable?) {}

        })
    }

    private fun enableValidateButton() {
        validatedButton.isEnabled = (dueIsNotEmpty && nameIsNotEmpty && amountIsNotEmpty)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //   viewModel = ViewModelProvider(this).get(NewOperationViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bill->{
                typeOps = 2L
                previsionSwitch.isChecked = !depenseSwitch.isChecked
            }
            R.id.preview->{
                typeOps = 1L
                depenseSwitch.isChecked = !previsionSwitch.isChecked
            }
            R.id.validated_button -> {
                dialog.show()
                val color = if (typeOps.toInt() == 1) "#537749" else "#887300"

                val newOperation = Operation(
                    0,
                    txtInputName.text.toString(),
                    txtInputDue.text.toString(),
                    txtInputAmount.text.toString().toFloat(),
                    color,
                    typeOps
                )

                Log.i("my name", newOperation.toString())

                viewModel.insert(newOperation)

                viewModel.insertResult.observe(this, Observer {
                    txtInputAmount.text!!.clear()
                    txtInputDue.text!!.clear()
                    txtInputName.text!!.clear()
                    Log.i("my news id", it.toString())
                    dialog.dismiss()
                })
                viewModel.allOperation.observe(
                    this,
                    Observer { ops ->
                        Log.i("my news ops", ops.toString())
                    })
            }
        }
    }

}