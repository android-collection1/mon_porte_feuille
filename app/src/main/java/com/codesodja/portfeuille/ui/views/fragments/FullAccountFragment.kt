package com.codesodja.portfeuille.ui.views.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codesodja.portfeuille.PortefeuilleApplication
import com.codesodja.portfeuille.R
import com.codesodja.portfeuille.ui.viewmodels.FullAccountViewModel
import com.codesodja.portfeuille.ui.viewmodels.FullAccountViewModelFactory
import com.sodja.rally.adapters.OperationListAdapter

class FullAccountFragment : Fragment() {

    companion object {
        fun newInstance() = FullAccountFragment()
    }

    private var status = 0
    private var total: Double = 0.0
    private lateinit var progressBar: ProgressBar
    private val handler = Handler()
    lateinit var textView: TextView

    private lateinit var viewModel: FullAccountViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fullAccountView = inflater.inflate(R.layout.full_account_fragment, container, false)

        // VIEW MODEL FACTORY INITIALIZR
        val typeOperationRepository =
            (requireActivity().application as PortefeuilleApplication).typeOperationRepository
        val operationRepository =
            (requireActivity().application as PortefeuilleApplication).operationRepository
        val factory = FullAccountViewModelFactory(typeOperationRepository, operationRepository)

        viewModel = ViewModelProvider(this, factory).get(FullAccountViewModel::class.java)

        val recyclerView = fullAccountView.findViewById<RecyclerView>(R.id.account_recyclerview)
        val accountAdapter = OperationListAdapter()
        recyclerView.adapter = accountAdapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())


        viewModel.account.observe(requireActivity(), Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let {listOperation->
                Log.d("account liste", listOperation[0].operations.toString())
                this.total = listOperation[0].operations.sumByDouble { it.amount.toDouble() }
                Log.d("total", this.total.toString())
                createAnimator(fullAccountView)
                accountAdapter.submitList(listOperation[0].operations)
            }
        })

        return fullAccountView
    }

    private fun createAnimator(fullAccountView: View) {
        Log.d("total++", this.total.toString())
        val resources = resources
        val drawable = resources.getDrawable(R.drawable.account_circularprogressbar, null)
        progressBar = fullAccountView.findViewById(R.id.circularProgressbar)
        progressBar.progress = 0
        progressBar.max = 300
        progressBar.progressDrawable = drawable
        textView = fullAccountView.findViewById(R.id.txt_account_card_total_amount)
        Thread {
            while (status < 300) {
                status += 5
                handler.post {
                    progressBar.progress = status
                    textView.text = "$total F CFA"
                }
                try {
                    Thread.sleep(16)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }.start()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(FullAccountViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

      /*  val resources = resources
        val drawable = resources.getDrawable(R.drawable.account_circularprogressbar, null)
        progressBar = view.findViewById(R.id.circularProgressbar)
        progressBar.progress = 0
        progressBar.secondaryProgress = 0
        progressBar.max = 1000
        progressBar.progressDrawable = drawable
        textView = view.findViewById(R.id.textView)
        Thread {
            while (status < 1000) {
                status += 5
                handler.post {
                    progressBar.progress = status
                    textView.text = String.format("%d%%", status)
                }
                try {
                    Thread.sleep(16)
                }
                catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }.start()*/
    }

}